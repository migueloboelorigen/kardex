import React from 'react'
import { Redirect, RouteMiddleware, Router } from 'react-typesafe-routes';
import { useAuth } from 'Views/AuthContext';
import { HomePage, ProductPage, LoginPage, SingUpPage,MovementsPage  } from './Views/Pages';

const AuthMiddleware: RouteMiddleware = (next) => {
	const { currentUser } = useAuth()
	if(!!currentUser) {
	  return next;
	} else {
	  return () => <Redirect to={routerBlank.login()} />;
	}
  };

export const router = Router(route => ({
	home: route('/', {
		component: HomePage,
		middleware: next => AuthMiddleware(next),
	}),
	products: route('products', {
		component: ProductPage,
	}),
	movements: route('movements', {
		component: MovementsPage,
	})
}));
export const routerBlank = Router(route => ({
	login: route('login', {
		component: LoginPage
	}),
	signup: route('signup', {
		component: SingUpPage,
	}),
}));
