import { Button, Grid, Typography } from "@material-ui/core";
import { Theme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { ProductDialog, ProductTable } from "../Components";

export function ProductPage() {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	const handleAddProduct = () => {
		setOpen(true);
	};
	
	return (
		<Grid container className={classes.root}>
			<ProductDialog open={open} onClose={handleClose} />
			<Grid item xs={6}>
				<Typography variant="h4" gutterBottom>
					Listado de productos
				</Typography>
			</Grid>
			<Grid item xs={6}>
				<div className={classes.buttonContainer}>

					<Button
					id="addProduct"
						className={classes.button}
						variant="contained"
						color="primary"
						onClick={handleAddProduct}
					>
						Agregar Producto
					</Button>
				</div>
			</Grid>
			<Grid item xs={12}>
				<ProductTable />
			</Grid>
		</Grid>
	);
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: 20,
		[theme.breakpoints.down("md")]: {
			paddingTop: 50,
			paddingLeft: 15,
			paddingRight: 15,
		},
	},

	buttonContainer: {
		width: "100%",
		display: "flex",
		justifyContent: "flex-end",
	},

	button: {
		marginBottom: 15,
		backgroundColor: '#193104 !important',
		color: 'white',
		borderRadius: 25,
		padding: '3px 15px',
		boxShadow: 'none',
		'&:hover': { backgroundColor: 'white !important', color: '#193104 !important' }
	},
}));
