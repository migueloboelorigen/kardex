import { Product } from '@Core/Domain/Entities/Product';
import { ConfigActions } from '../Types/config';
import {  ProductAction, ProductActions } from '../Types/index';
import createReducer from './createReducer';

export interface IStateProducts {
	ListProducts: Product[]
	product:Product | null 
}
const initialState: IStateProducts = {
	ListProducts:[],
	product:null
}
export const productList = createReducer<IStateProducts>(initialState, {
	[ProductActions.ADD_PRODUCT](state: IStateProducts, action: ProductAction) {
		//@ts-ignore
		return {...state, ListProducts: [...action.payload]}
	},
	[ProductActions.GET_PRODUCT](state: Product, action: ProductAction) {
		return {...state, product:action.payload}
	},
	[ProductActions.GETS_PRODUCTS](state: IStateProducts, action: ProductAction) {
		//@ts-ignore
		return {...state, ListProducts: [...action.payload]}
	},
	[ProductActions.DELETE_PRODUCT](state: IStateProducts, action: ProductAction) {
		return {...state, ListProducts: action.payload}
	},
	[ConfigActions.PURGE_STATE](state: IStateProducts, action: ProductAction) {
		return {...state};
	},
});
