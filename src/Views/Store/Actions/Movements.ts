import { MovementsAction, MovementsActions } from '../Types/index';
import { MovementsRepositoryImpl } from '@Core/Infrastructure/Repositories/Movements/MovementsRepositoryImpl';
import { MovementsService } from '@Core/Usecases/MovementsService';
import { Movements } from '../../../Core/Domain/Entities/Movements';

interface Imovement {
	id: string;
	type: string;
	name: string;
	cant: number;
	idProduct: string;
}

export const addMovement = (movement: Imovement): Promise<MovementsAction> => {
	// @ts-ignore
	return async function saveNewTodoThunk(dispatch) {
		const mapperData = {
			id: movement.id,
			type: movement.type,
			name: movement.name,
			cant: movement.cant,
			idProduct: movement.idProduct
		};
		const movementInstance = new Movements(mapperData);
		const movementRepo = new MovementsRepositoryImpl();
		const movementService = new MovementsService(movementRepo);

		await movementService.addMovements(movementInstance);
		const ListMovementss = await movementService.getMovements();
		dispatch({ type: MovementsActions.ADD_MOVEMENTS, payload: ListMovementss });
	};
};

export const getMovements = (): Promise<MovementsAction> => {
	// @ts-ignore
	return async function saveNewTodoThunk(dispatch) {
		const movementRepo = new MovementsRepositoryImpl();
		const movementService = new MovementsService(movementRepo);
		const ListMovementss = await movementService.getMovements();
		dispatch({ type: MovementsActions.GETS_MOVEMENTSS, payload: ListMovementss });
	};
}
