import { ProductAction, ProductActions } from '../Types/index';
import { ProductRepositoryImpl } from '@Core/Infrastructure/Repositories/Product/ProductRepositoryImpl';
import { ProductsService } from '@Core/Usecases/ProductsService';
import { Product } from '../../../Core/Domain/Entities/Product';

interface Iproduct {
	id: string;
	name: string;
	description: string;
	price: number;
	code: string;
}

export const addProduct = (product: Iproduct): Promise<ProductAction> => {
	// @ts-ignore
	return async function saveNewTodoThunk(dispatch) {
		const mapperData = {
			id: product.id,
			name: product.name,
			description: product.description,
			price: product.price,
			code: product.code,
			stock: 0,
		};
		const productInstance = new Product(mapperData);
		const productRepo = new ProductRepositoryImpl();
		const productService = new ProductsService(productRepo);

		await productService.addProduct(productInstance);
		const ListProducts = await productService.getProducts();
		dispatch({ type: ProductActions.ADD_PRODUCT, payload: ListProducts });
	};
};

export const getProducts = (): Promise<ProductAction> => {
	// @ts-ignore
	return async function saveNewTodoThunk(dispatch) {
		const productRepo = new ProductRepositoryImpl();
		const productService = new ProductsService(productRepo);
		const ListProducts = await productService.getProducts();
		dispatch({ type: ProductActions.GETS_PRODUCTS, payload: ListProducts });
	};
}

// export function uncompleteProduct(productId: number): ProductAction {
// 	return {
// 		type: ProductActions.GET_PRODUCT,
// 		payload: productId,
// 	};
// }

// export function deleteProduct(productId: number): ProductAction {
// 	return {
// 		type: ProductActions.DELETE_PRODUCT,
// 		payload: productId,
// 	};
// }
