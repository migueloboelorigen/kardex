// prettier-ignore
import { AppBar, IconButton, Toolbar, Typography, useMediaQuery, Grid} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { Theme, useTheme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import * as React from 'react';
import { useSelector } from 'react-redux';
import { Router } from 'react-router-dom';
import { RouterSwitch } from 'react-typesafe-routes';
import { history } from './configureStore';
import { router } from './Router';
import { Drawer } from './Views/Components/Drawer';
import { Snackbar } from './Views/Components/Snackbar';
import { useActions } from './Views/Store/Actions';
import * as ConfigActions from './Views/Store/Actions/config';
import { RootState } from './Views/Store/Reducers';
import { withRoot } from './withRoot';
import { AuthProvider } from 'Views/AuthContext';
import MenuLogout from './Views/Components/Menu';

function App() {
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
	const classes = useStyles();

	const drawerOpen: boolean = useSelector((state: RootState) => state.drawerOpen);
	const configActions: typeof ConfigActions = useActions(ConfigActions);
	const handleDrawerToggle = () => {
		configActions.setDrawerOpen(!drawerOpen);
	};

	return (
		<Router history={history}>
			<AuthProvider>
				<div className={classes.root}>
					<div className={classes.appFrame}>
						<Snackbar />
						<AppBar className={classes.appBar}>
							<Toolbar>
								<Grid
									container
									spacing={1}
									direction="row"
									justify="flex-start"
									alignItems="flex-start"
									alignContent="stretch"
									wrap="nowrap"
								>
									<Grid
										container
										spacing={1}
									>
										<IconButton
											color="inherit"
											aria-label="open drawer"
											onClick={handleDrawerToggle}
											className={classes.navIconHide}
										>
											<MenuIcon />
										</IconButton>
										<Typography className={classes.textLogo} variant="h6" color="inherit" noWrap={isMobile}>
											Hulk Store
										</Typography>
									</Grid>
									<Grid
										container
										spacing={1}
										direction="row"
										justify="flex-start"
										alignItems="flex-start"
										alignContent="stretch"
										wrap="nowrap"
									>
										
											<MenuLogout />
										
									</Grid>
								</Grid>
							</Toolbar>
						</AppBar>
						<Drawer />
						<div className={classes.content}>
							<RouterSwitch router={router} />
						</div>
					</div>
				</div>
			</AuthProvider>
		</Router>
	);
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: '100%',
		height: '100%',
		zIndex: 1,
		overflow: 'hidden',
	},
	appFrame: {
		position: 'relative',
		display: 'flex',
		width: '100%',
		height: '100%',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		position: 'absolute',
		backgroundColor: '#193104 !important',
	},
	textLogo:{
		position: "relative",
		top: 7,
	},
	navIconHide: {
		
		backgroundColor: '#193104 !important',
		[theme.breakpoints.up('md')]: {
			display: 'none',
		},
	},
	content: {
		backgroundColor: theme.palette.background.default,
		width: '100%',
		height: 'calc(100% - 56px)',
		marginTop: 56,
		[theme.breakpoints.up('sm')]: {
			height: 'calc(100% - 64px)',
			marginTop: 64,
		},
	},
}));

export default withRoot(App);
