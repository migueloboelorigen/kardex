import { Product } from "@Entities/Product";

export interface ProductRepository {
    getProducts: ()=> Promise<Product[]>
    addProduct(data: Product): Promise<Product | undefined>
}