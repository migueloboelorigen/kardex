// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import firebase from "firebase/compat/app"
import "firebase/compat/auth"
import "firebase/compat/database"
import "firebase/compat/firestore"

import { attachCustomCommands } from "cypress-firebase"




const firebaseConfig = {
	apiKey: "AIzaSyAf3NFOnHMdIv_ojrzhZ-KLFNlIdJTqTHw",
	authDomain: "kardex-19ca0.firebaseapp.com",
	databaseURL: "https://kardex-19ca0-default-rtdb.firebaseio.com",
	projectId: "kardex-19ca0",
	storageBucket: "kardex-19ca0.appspot.com",
	messagingSenderId: "731468953704",
	appId: "1:731468953704:web:e2661d7eeb053c36a1e3dd",
	measurementId: "G-5LPCDPGFT4"
	
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

attachCustomCommands({Cypress, cy,firebase })

Cypress.Commands.add("testCommand", () => {
	cy.server({ force404: true });
	cy.clearLocalStorage();
});